package com.muhardin.belajar.designpattern.bridge;

import java.io.File;

public interface FileProcessor {
    File write(String nama, File data);
    File read(String nama);
}
