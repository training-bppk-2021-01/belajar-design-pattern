package com.muhardin.belajar.designpattern.bridge;

import java.io.File;

public class LocalFileService extends FileService {

    public LocalFileService(FileProcessor fileStorage) {
        super(fileStorage);
    }

    public String simpan(File file){
        File hasilProses = getFileStorage().write(file.getName(), file);
        System.out.println("Simpan file di local");
        return null;
    }

    public File ambil(String lokasi){
        return getFileStorage().read(lokasi);
    }
}
