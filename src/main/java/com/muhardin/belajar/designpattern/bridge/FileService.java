package com.muhardin.belajar.designpattern.bridge;

import java.io.File;

public abstract class FileService {

    private FileProcessor fileStorage;

    public FileService(FileProcessor fileStorage) {
        this.fileStorage = fileStorage;
    }

    public FileProcessor getFileStorage(){
        return fileStorage;
    }

    public abstract String simpan(File file);
    public abstract File ambil(String lokasi);
}
