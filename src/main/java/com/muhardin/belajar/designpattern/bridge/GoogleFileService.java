package com.muhardin.belajar.designpattern.bridge;

import java.io.File;

public class GoogleFileService extends FileService {

    public GoogleFileService(FileProcessor fileStorage) {
        super(fileStorage);
        
    }

    @Override
    public String simpan(File file) {
        File hasil = getFileStorage().write(file.getName(), file);
        System.out.println("Upload ke google");
        return null;
    }

    @Override
    public File ambil(String lokasi) {
        // TODO Auto-generated method stub
        return null;
    }
    
}
