package com.muhardin.belajar.designpattern.bridge;

import java.io.File;

public class PlainStorage implements FileProcessor {

    public File write(String nama, File data) {
        System.out.println("Langsung kembalikan apa adanya");
        return data;
    }

    public File read(String nama) {
        // TODO Auto-generated method stub
        return null;
    }
    
}
