package com.muhardin.belajar.designpattern.bridge;

import java.io.File;

public class FileExplorerController {

    private FileService fileService;
    private FileProcessor fileProcessor;

    public FileExplorerController(){
        // interface tidak bisa diinstankan, 
        // karena methodnya abstract
        //fileService = new FileService();
        
        inisialisasiFileServiceProcessor();
    }

    // ini nanti bisa dihandle pakai SPRING_PROFILES_ACTIVE
    private void inisialisasiFileServiceProcessor() {
        // bila mau simpan plain di local
        // SPRING_PROFILES_ACTIVE=local,plain
        fileProcessor = new PlainStorage();
        fileService = new LocalFileService(fileProcessor);

        // bila mau simpan encrypted di local
        // SPRING_PROFILES_ACTIVE=local,encrypted
        fileProcessor = new EncryptedStorage();
        fileService = new LocalFileService(fileProcessor);

        // bila mau simpan plain di google
        // SPRING_PROFILES_ACTIVE=google,plain
        fileProcessor = new PlainStorage();
        fileService = new GoogleFileService(fileProcessor);

        // bila mau simpan encrypted di google
        // SPRING_PROFILES_ACTIVE=google,encrypted
        fileProcessor = new EncryptedStorage();
        fileService = new GoogleFileService(fileProcessor);
    }

    public void displayFiles(){
        File hasil = fileService.ambil("/data/endy.txt");
    }
}
