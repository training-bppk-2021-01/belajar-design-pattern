package com.muhardin.belajar.designpattern.singleton;


// Cara membuat singleton
// memaksa orang menginstankan berkali-kali
public class KoneksiDatabasePrototype {

    private String namaDatabase;

    public String getNamaDatabase(){
        return namaDatabase;
    }

    // 1. Constructornya diset private
    private KoneksiDatabasePrototype(String namaDb) {
        System.out.println("Menginstankan objek koneksi database");
        namaDatabase = namaDb;
    }

    private static KoneksiDatabasePrototype kd;

    // 2. Sediakan method static untuk membuatkan object
    public static KoneksiDatabasePrototype getKoneksiDatabase(String nama) {
        return new KoneksiDatabasePrototype(nama);
    }
}
