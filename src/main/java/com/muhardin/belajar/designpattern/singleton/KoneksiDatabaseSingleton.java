package com.muhardin.belajar.designpattern.singleton;


// Cara membuat singleton
// melarang orang menginstankan berkali-kali
public class KoneksiDatabaseSingleton {

    private String namaDatabase;

    public String getNamaDatabase(){
        return namaDatabase;
    }

    // 1. Constructornya diset private
    private KoneksiDatabaseSingleton(String namaDb) {
        System.out.println("Menginstankan objek koneksi database");
        namaDatabase = namaDb;
    }

    private static KoneksiDatabaseSingleton kd;

    // 2. Sediakan method static untuk membuatkan object
    public static KoneksiDatabaseSingleton getKoneksiDatabase(String nama) {
        if(kd == null) {
            kd = new KoneksiDatabaseSingleton(nama);
        }
        return kd;
    }
}
