package com.muhardin.belajar.designpattern.singleton;

public class DemoSingleton {
    public static void main(String[] args) {

        // tidak bisa digunakan, karena constructor diset private
        // KoneksiDatabase kd = new KoneksiDatabase();
        // KoneksiDatabase kd1 = new KoneksiDatabase();

        // menggunakan singleton
        KoneksiDatabaseSingleton kd = KoneksiDatabaseSingleton.getKoneksiDatabase("trainingdb");
        System.out.println("Nama database kd: "+kd.getNamaDatabase());

        KoneksiDatabaseSingleton kd1 = KoneksiDatabaseSingleton.getKoneksiDatabase("pelatihandb");
        System.out.println("Nama database kd1: "+kd1.getNamaDatabase());
    }
}
