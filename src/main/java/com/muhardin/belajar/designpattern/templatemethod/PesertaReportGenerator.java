package com.muhardin.belajar.designpattern.templatemethod;

public class PesertaReportGenerator extends AbstractReportGenerator {

    @Override
    public String getTemplate() {
        return "template report peserta";
    }

    @Override
    public String fillData(String template) {
        return "select * from peserta";
    }
    
}
