package com.muhardin.belajar.designpattern.templatemethod;

public class MateriController {
    private MateriReportGenerator reportGenerator;

    public void generateReport(){
        reportGenerator.generateReport();
    }
}
