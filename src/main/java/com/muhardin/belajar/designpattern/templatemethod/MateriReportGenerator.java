package com.muhardin.belajar.designpattern.templatemethod;

public class MateriReportGenerator extends AbstractReportGenerator {

    @Override
    public String getTemplate() {
        return "template report materi";
    }

    @Override
    public String fillData(String template) {
        // TODO Auto-generated method stub
        return "Fill materi template dengan data";
    }

    public Object generateReport() {
        return null;
    }
    
}
