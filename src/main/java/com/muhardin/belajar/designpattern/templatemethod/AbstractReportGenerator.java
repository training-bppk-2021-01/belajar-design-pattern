package com.muhardin.belajar.designpattern.templatemethod;

// menjadi template untuk membuat report generator
// sesuai tabel database
public abstract class AbstractReportGenerator {

    public abstract String getTemplate();
    public abstract String fillData(String template);
    
    private Object exportReport(String report){
        return null;
    }

    // urutan kerja harus begini, tidak boleh dioverride
    public final Object generateReport(){
        String template = getTemplate();
        String reportWithData = fillData(template);
        return exportReport(reportWithData);
    }
}
