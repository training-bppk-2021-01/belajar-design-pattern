package com.muhardin.belajar.designpattern.builder;

import java.time.LocalDate;

public class DemoBuilder {
    public static void main(String[] args) {
        Peserta p = Peserta.builder()
        .nama("Endy")
        .tanggalLahir(LocalDate.of(2022, 07, 01))
        .build();

        System.out.println("Nama Peserta : "+p.getNama());
        System.out.println("Tanggal lahir : "+p.getTanggalLahir());
    }
}
