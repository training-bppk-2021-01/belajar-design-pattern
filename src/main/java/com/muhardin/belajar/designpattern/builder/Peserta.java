package com.muhardin.belajar.designpattern.builder;

import java.time.LocalDate;

public class Peserta {
    private String nama;
    private LocalDate tanggalLahir;

    private Peserta(){

    }

    public String getNama(){
        return nama;
    }

    private void setNama(String nama) {
        this.nama = nama;
    }

    public LocalDate getTanggalLahir(){
        return tanggalLahir;
    }

    private void setTanggalLahir(LocalDate tanggal) {
        tanggalLahir = tanggal;
    }

    public static PesertaBuilder builder() {
        return new PesertaBuilder();
    }

    static class PesertaBuilder {
        private Peserta hasil = new Peserta();

        public PesertaBuilder nama(String nama){
            hasil.setNama(nama);
            return this;
        }

        public PesertaBuilder tanggalLahir(LocalDate tanggal) {
            hasil.setTanggalLahir(tanggal);
            return this;
        }
        
        public Peserta build(){
            validasi();
            return hasil;
        }

        private void validasi() {
            if(hasil.getTanggalLahir().isAfter(LocalDate.now())) {
                throw new IllegalStateException("Tanggal lahir harus sebelum hari ini");
            }
        }
    }
}
